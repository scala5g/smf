import express, { response } from "express";
import bodyParser from 'body-parser';
import axios from 'axios';

const app = express()

app.use(express.urlencoded({ extended: true }));
app.use(express.json());

function makeid(length: number): string {
    let result = '';
    const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    const charactersLength = characters.length;
    for (let i = 0; i < length; i++) {
        result += characters.charAt(Math.floor(Math.random() *
            charactersLength));
    }
    return result;
}

function createpdu(userid: string): string {
    return makeid(10)
}
app.post("/createpdu", async (req, res) => {
    const sessionID: string = createpdu(req.body.msisdn)
    await axios.post('http://upf-service:8321/addsession', {}).then(upfresponse => {
        res.writeHead(200, { 'Content-Type': 'text/plain' });
        res.write("smf:session_created:" + sessionID)
        res.end()
    }).catch(error => {
        console.log("smf error on axios")
        res.status(503)
        res.end("smf:error on upf server")
    })

})

app.listen(7323, () => {
    console.log("Init smf")
})